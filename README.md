# MonitoringPlug Test Data

## http

Contains the test data for curl based checks.

* docker run -d -p 8080:80 --rm -n testdata-http registry.gitlab.com/monitoringplug/testdata/http:latest

## snmp

Contains SNMP walks for use with [replayd](https://gitlab.com/monitoringplug/replayd).

* docker run -d p 1661:1661/udp --rm --name testdata-snmp registry.gitlab.com/monitoringplug/testdata/snmp:latest
